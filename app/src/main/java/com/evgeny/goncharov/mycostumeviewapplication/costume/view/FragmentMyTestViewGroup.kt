package com.evgeny.goncharov.mycostumeviewapplication.costume.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.evgeny.goncharov.mycostumeviewapplication.R

class FragmentMyTestViewGroup : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_test_view_group, null, false)
    }


}