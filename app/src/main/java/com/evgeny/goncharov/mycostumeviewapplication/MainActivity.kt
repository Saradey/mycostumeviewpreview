package com.evgeny.goncharov.mycostumeviewapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.evgeny.goncharov.mycostumeviewapplication.costume.view.FragmentMyTestViewGroup
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnFirst.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frmContainer, FragmentMyTestViewGroup())
                .commit()
        }
    }


}
