package com.evgeny.goncharov.mycostumeviewapplication.costume.view

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StyleRes
import androidx.core.content.res.use
import androidx.core.view.*
import com.evgeny.goncharov.mycostumeviewapplication.R

/**
 * ViewGroup которая имеет четыре стороны на которых размещаются View чайлды
 * также чайлды могут размещаться относительно чего то как констреинт
 */

class MyTestViewGroup : ViewGroup {

    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet, @StyleRes style: Int) : super(
        context,
        attributeSet,
        style
    ) {
        init(context, attributeSet)
    }

    private var withSize = 0
    private var heightSize = 0


    private fun init(context: Context, attributeSet: AttributeSet) {

    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        withSize = MeasureSpec.getSize(widthMeasureSpec)
        heightSize = MeasureSpec.getSize(heightMeasureSpec)
        if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.UNSPECIFIED) {
            withSize *= 2
            setMeasuredDimension(withSize, heightSize)
        }
        measureChild()
    }


    private fun measureChild() {
        children.forEach { child ->
            measureChild(
                child,
                getMeasure(withSize),
                getMeasure(heightSize)
            )
        }
    }


    private fun getMeasure(value: Int): Int {
        return MeasureSpec.makeMeasureSpec(value, MeasureSpec.EXACTLY)
    }


    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        children.forEach { child ->
            val param = child.layoutParams as MyTestViewGroup
            when {
                (param.layoutGravity == 1) -> {
                    val xPosition = withSize / 2
                    val yPosition = 0
                    child.layout(
                        xPosition - child.measuredWidth / 2,
                        yPosition,
                        (xPosition - child.measuredWidth / 2) + child.measuredWidth,
                        yPosition + child.measuredHeight
                    )
                }
                (param.idConstraint == -1) && (param.absoluteX == 0 || param.absoluteY == 0) -> {
                    child.layout(
                        0,
                        0,
                        0 + child.measuredWidth,
                        0 + child.measuredHeight
                    )
                }
                (param.absoluteX != 0 || param.absoluteY != 0) -> {
                    child.layout(
                        param.absoluteX,
                        param.absoluteY,
                        param.absoluteX + child.measuredWidth,
                        param.absoluteY + child.measuredHeight
                    )
                }
                (param.idConstraint != -1) -> {
                    children.forEach { second ->
                        if (second.id == param.idConstraint) {
                            val paramSecond = second.layoutParams as MyTestViewGroup
                            if (paramSecond.absoluteX != 0 && paramSecond.absoluteY != 0) {
                                when {
                                    param.constraintBottom -> {
                                        child.layout(
                                            paramSecond.absoluteX,
                                            second.measuredHeight + paramSecond.absoluteY,
                                            paramSecond.absoluteX + child.measuredWidth,
                                            second.measuredHeight + paramSecond.absoluteY + child.measuredHeight
                                        )
                                    }
                                    param.constraintLeft -> {
                                        child.layout(
                                            paramSecond.absoluteX - child.measuredWidth,
                                            paramSecond.absoluteY,
                                            paramSecond.absoluteX + child.measuredWidth,
                                            paramSecond.absoluteY + child.measuredHeight
                                        )
                                    }
                                    param.constraintRight -> {
                                        child.layout(
                                            paramSecond.absoluteX + second.measuredWidth,
                                            paramSecond.absoluteY,
                                            paramSecond.absoluteX + child.measuredWidth + second.measuredWidth,
                                            paramSecond.absoluteY + child.measuredHeight
                                        )
                                    }
                                    param.constraintTop -> {
                                        child.layout(
                                            paramSecond.absoluteX,
                                            paramSecond.absoluteY - child.measuredHeight,
                                            paramSecond.absoluteX + child.measuredWidth,
                                            paramSecond.absoluteY + child.measuredHeight
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
    }


    override fun generateLayoutParams(attrs: AttributeSet): LayoutParams {
        return MyTestViewGroup(context, attrs)
    }


    override fun generateLayoutParams(param: LayoutParams): LayoutParams {
        return MyTestViewGroup(param)
    }


    override fun generateDefaultLayoutParams(): LayoutParams {
        return MyTestViewGroup(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
    }


    override fun checkLayoutParams(p: LayoutParams): Boolean {
        return p is MyTestViewGroup
    }


    class MyTestViewGroup : MarginLayoutParams {
        companion object {
            const val LAYOUT_TOP_LEFT = 0
            const val LAYOUT_TOP_RIGHT = 1
            const val LAYOUT_BOTTOM_LEFT = 2
            const val LAYOUT_BOTTOM_RIGHT = 3
        }

        var idConstraint = -1
        var constraintBottom = false
        var constraintTop = false
        var constraintLeft = false
        var constraintRight = false
        var layoutSide = LAYOUT_TOP_LEFT
        var layoutGravity = -1
        var absoluteX = 0
        var absoluteY = 0


        constructor(context: Context, attrSet: AttributeSet) : super(context, attrSet) {
            init(context, attrSet)
        }

        constructor(width: Int, height: Int) : super(width, height)

        constructor(lp: ViewGroup.LayoutParams) : super(lp)


        private fun init(context: Context, attrSet: AttributeSet) {
            val typeAdapter =
                context.obtainStyledAttributes(attrSet, R.styleable.MyTestViewGroup_Layout)
            typeAdapter.use {
                layoutSide = it.getInt(
                    R.styleable.MyTestViewGroup_Layout_layout_side,
                    LAYOUT_TOP_LEFT
                )
                layoutGravity = it.getInt(
                    R.styleable.MyTestViewGroup_Layout_layout_gravity,
                    -1
                )
                absoluteX = it.getInt(
                    R.styleable.MyTestViewGroup_Layout_layout_absolutePositionPixelX,
                    0
                )
                absoluteY = it.getInt(
                    R.styleable.MyTestViewGroup_Layout_layout_absolutePositionPixelY,
                    0
                )
                when {
                    getValueConstraintBottom(it) != -1 -> {
                        idConstraint = getValueConstraintBottom(it)
                        constraintBottom = true
                    }
                    getValueConstraintTop(it) != -1 -> {
                        idConstraint = getValueConstraintTop(it)
                        constraintTop = true
                    }
                    getValueConstraintLeft(it) != -1 -> {
                        idConstraint = getValueConstraintLeft(it)
                        constraintLeft = true
                    }
                    getValueConstraintRight(it) != -1 -> {
                        idConstraint = getValueConstraintRight(it)
                        constraintRight = true
                    }
                }
            }
        }


        private fun getValueConstraintBottom(typeAdapter: TypedArray): Int {
            return typeAdapter.getResourceId(
                R.styleable.MyTestViewGroup_Layout_layout_constraintBottom,
                -1
            )
        }

        private fun getValueConstraintTop(typeAdapter: TypedArray): Int {
            return typeAdapter.getResourceId(
                R.styleable.MyTestViewGroup_Layout_layout_constraintTop,
                -1
            )
        }

        private fun getValueConstraintLeft(typeAdapter: TypedArray): Int {
            return typeAdapter.getResourceId(
                R.styleable.MyTestViewGroup_Layout_layout_constraintLeft,
                -1
            )
        }

        private fun getValueConstraintRight(typeAdapter: TypedArray): Int {
            return typeAdapter.getResourceId(
                R.styleable.MyTestViewGroup_Layout_layout_constraintRight,
                -1
            )
        }
    }

}